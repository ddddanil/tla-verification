---- MODULE ModelChecker ----
EXTENDS TaskScheduler

ProcPrioritiesVal == << 1, 1 >>
ProcessesBasicVal == 1
ProcessesAdvancedVal == 1
MaxReadyProcessesVal == 1
MaxPriorityVal == 3

OnlySingleProcess ==
    LET IsRunning(x) == x = "running"
    IN
    Len(SelectSeq(processState, IsRunning)) <= 1

ReadyProcessesNoMoreThanMax ==
    LET IsReady(x) == x = "ready"
    IN
    Len(SelectSeq(processState, IsReady)) <= MaxReadyProcesses

StateValueCheck ==
    LET ValidStates == <<"suspended", "ready", "running", "waiting">>
    IN
    \A procState \in DOMAIN processState: 
        \E validState \in DOMAIN ValidStates:
            processState[procState] = ValidStates[validState]

PriorityCannotWait ==
[][~\E i,j \in DOMAIN ProcPriorities: 
    /\ ProcPriorities[i] < ProcPriorities[j]
    /\ processState[i] = "running"
    /\ processState[j] = "ready"
    /\ \/ processState[i]' = "suspended"
       \/ processState[i]' = "waiting"]_processState

====