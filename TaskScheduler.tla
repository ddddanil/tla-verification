-------------------------------- MODULE TaskScheduler --------------------------------
EXTENDS Integers, Naturals, Sequences, TLC

CONSTANT MaxReadyProcesses, ProcessesAdvanced, ProcessesBasic, ProcPriorities, MaxPriority
ASSUME Len(ProcPriorities) = ProcessesAdvanced + ProcessesBasic

(*--algorithm task_scheduler 
    variables
        readyProcesses = 0;
        runningProcess = 0;
        processState = [j \in 1..(ProcessesAdvanced + ProcessesBasic) |-> "suspended"];
        prioritiesQueue = [j \in 0..MaxPriority |-> <<>>];

    define
        CanBeReady == readyProcesses < MaxReadyProcesses

        NoProcessesWithHigherPriority(self) == \A j \in (ProcPriorities[self] + 1)..MaxPriority : Len(prioritiesQueue[j]) = 0

        IsHeadOfQueue(self) == Head(prioritiesQueue[ProcPriorities[self]]) = self

        CanActivate(self) == /\ processState[self] = "suspended" 
                            /\ CanBeReady

        CanStart(self) == /\ processState[self] = "ready"
                        /\ runningProcess = 0
                        /\ IsHeadOfQueue(self)
                        /\ NoProcessesWithHigherPriority(self)

        CanTerminate(self) == /\ processState[self] = "running"
                            /\ NoProcessesWithHigherPriority(self)

        CanPreempt(self) == /\ processState[self] = "ready"
                            /\ IsHeadOfQueue(self)
                            /\ NoProcessesWithHigherPriority(self)
                            /\ runningProcess # 0
                            /\ ProcPriorities[runningProcess] < ProcPriorities[self]

        CanWait(self) == /\ processState[self] = "running"
                        /\ NoProcessesWithHigherPriority(self)

        CanRelease(self) == /\ processState[self] = "waiting"
                            /\ CanBeReady
    end define;

    macro doActivate(self) begin
        await CanActivate(self);

        processState[self] := "ready";

        prioritiesQueue[ProcPriorities[self]] := Append(prioritiesQueue[ProcPriorities[self]], self);
        readyProcesses := readyProcesses + 1;
    end macro;

    macro doStart(self) begin
        await CanStart(self);
        
        processState[self] := "running";
        
        prioritiesQueue[ProcPriorities[self]] := Tail(prioritiesQueue[ProcPriorities[self]]);
        readyProcesses := readyProcesses - 1;
        
        runningProcess := self;
    end macro;

    macro doTerminate(self) begin
        await CanTerminate(self);
        
        processState[self] := "suspended";

        runningProcess := 0;
    end macro;

    macro doPreempt(self) begin
        await CanPreempt(self);
        
        processState[runningProcess] := "ready" ||
        processState[self] := "running";

        prioritiesQueue[ProcPriorities[runningProcess]] := Append(prioritiesQueue[ProcPriorities[runningProcess]], runningProcess) ||
        prioritiesQueue[ProcPriorities[self]] := Tail(prioritiesQueue[ProcPriorities[self]]);
        runningProcess := self;
    end macro;

    macro doWait(self) begin
        await CanWait(self);
        
        processState[self] := "waiting";

        runningProcess := 0;
    end macro;

    macro doRelease(self) begin
        await CanRelease(self);

        processState[self] := "ready";

        prioritiesQueue[ProcPriorities[self]] := Append(prioritiesQueue[ProcPriorities[self]], self);
        readyProcesses := readyProcesses + 1;
    end macro;

    process baseBehaviour \in 1..(ProcessesBasic)
    begin
        baseLoop:
        while (TRUE) do
            either
                doActivate(self);
            or
                doStart(self);
            or
                doTerminate(self);
            or
                doPreempt(self);
            end either;
        end while;
    end process;
    
    process advancedBehaviour \in (ProcessesBasic + 1)..(ProcessesAdvanced + ProcessesBasic)
    begin
        advancedLoop:
        while (TRUE) do
            either
                doActivate(self);
            or
                doStart(self);
            or
                doTerminate(self);
            or
                doPreempt(self);
            or
                doWait(self);
            or
                doRelease(self);
            end either;
        end while;
    end process;
end algorithm;
*)
\* BEGIN TRANSLATION (chksum(pcal) = "c344c022" /\ chksum(tla) = "6a05e1ea")
VARIABLES readyProcesses, runningProcess, processState, prioritiesQueue, pc

(* define statement *)
CanBeReady == readyProcesses < MaxReadyProcesses

NoProcessesWithHigherPriority(self) == \A j \in (ProcPriorities[self] + 1)..MaxPriority : Len(prioritiesQueue[j]) = 0

IsHeadOfQueue(self) == Head(prioritiesQueue[ProcPriorities[self]]) = self

CanActivate(self) == /\ processState[self] = "suspended"
                    /\ CanBeReady

CanStart(self) == /\ processState[self] = "ready"
                /\ runningProcess = 0
                /\ IsHeadOfQueue(self)
                /\ NoProcessesWithHigherPriority(self)

CanTerminate(self) == /\ processState[self] = "running"
                    /\ NoProcessesWithHigherPriority(self)

CanPreempt(self) == /\ processState[self] = "ready"
                    /\ IsHeadOfQueue(self)
                    /\ NoProcessesWithHigherPriority(self)
                    /\ runningProcess # 0
                    /\ ProcPriorities[runningProcess] < ProcPriorities[self]

CanWait(self) == /\ processState[self] = "running"
                /\ NoProcessesWithHigherPriority(self)

CanRelease(self) == /\ processState[self] = "waiting"
                    /\ CanBeReady


vars == << readyProcesses, runningProcess, processState, prioritiesQueue, pc
        >>

ProcSet == (1..(ProcessesBasic)) \cup ((ProcessesBasic + 1)..(ProcessesAdvanced + ProcessesBasic))

Init == (* Global variables *)
        /\ readyProcesses = 0
        /\ runningProcess = 0
        /\ processState = [j \in 1..(ProcessesAdvanced + ProcessesBasic) |-> "suspended"]
        /\ prioritiesQueue = [j \in 0..MaxPriority |-> <<>>]
        /\ pc = [self \in ProcSet |-> CASE self \in 1..(ProcessesBasic) -> "baseLoop"
                                        [] self \in (ProcessesBasic + 1)..(ProcessesAdvanced + ProcessesBasic) -> "advancedLoop"]

baseLoop(self) == /\ pc[self] = "baseLoop"
                  /\ \/ /\ CanActivate(self)
                        /\ processState' = [processState EXCEPT ![self] = "ready"]
                        /\ prioritiesQueue' = [prioritiesQueue EXCEPT ![ProcPriorities[self]] = Append(prioritiesQueue[ProcPriorities[self]], self)]
                        /\ readyProcesses' = readyProcesses + 1
                        /\ UNCHANGED runningProcess
                     \/ /\ CanStart(self)
                        /\ processState' = [processState EXCEPT ![self] = "running"]
                        /\ prioritiesQueue' = [prioritiesQueue EXCEPT ![ProcPriorities[self]] = Tail(prioritiesQueue[ProcPriorities[self]])]
                        /\ readyProcesses' = readyProcesses - 1
                        /\ runningProcess' = self
                     \/ /\ CanTerminate(self)
                        /\ processState' = [processState EXCEPT ![self] = "suspended"]
                        /\ runningProcess' = 0
                        /\ UNCHANGED <<readyProcesses, prioritiesQueue>>
                     \/ /\ CanPreempt(self)
                        /\ processState' = [processState EXCEPT ![runningProcess] = "ready",
                                                                ![self] = "running"]
                        /\ prioritiesQueue' = [prioritiesQueue EXCEPT ![ProcPriorities[runningProcess]] = Append(prioritiesQueue[ProcPriorities[runningProcess]], runningProcess),
                                                                      ![ProcPriorities[self]] = Tail(prioritiesQueue[ProcPriorities[self]])]
                        /\ runningProcess' = self
                        /\ UNCHANGED readyProcesses
                  /\ pc' = [pc EXCEPT ![self] = "baseLoop"]

baseBehaviour(self) == baseLoop(self)

advancedLoop(self) == /\ pc[self] = "advancedLoop"
                      /\ \/ /\ CanActivate(self)
                            /\ processState' = [processState EXCEPT ![self] = "ready"]
                            /\ prioritiesQueue' = [prioritiesQueue EXCEPT ![ProcPriorities[self]] = Append(prioritiesQueue[ProcPriorities[self]], self)]
                            /\ readyProcesses' = readyProcesses + 1
                            /\ UNCHANGED runningProcess
                         \/ /\ CanStart(self)
                            /\ processState' = [processState EXCEPT ![self] = "running"]
                            /\ prioritiesQueue' = [prioritiesQueue EXCEPT ![ProcPriorities[self]] = Tail(prioritiesQueue[ProcPriorities[self]])]
                            /\ readyProcesses' = readyProcesses - 1
                            /\ runningProcess' = self
                         \/ /\ CanTerminate(self)
                            /\ processState' = [processState EXCEPT ![self] = "suspended"]
                            /\ runningProcess' = 0
                            /\ UNCHANGED <<readyProcesses, prioritiesQueue>>
                         \/ /\ CanPreempt(self)
                            /\ processState' = [processState EXCEPT ![runningProcess] = "ready",
                                                                    ![self] = "running"]
                            /\ prioritiesQueue' = [prioritiesQueue EXCEPT ![ProcPriorities[runningProcess]] = Append(prioritiesQueue[ProcPriorities[runningProcess]], runningProcess),
                                                                          ![ProcPriorities[self]] = Tail(prioritiesQueue[ProcPriorities[self]])]
                            /\ runningProcess' = self
                            /\ UNCHANGED readyProcesses
                         \/ /\ CanWait(self)
                            /\ processState' = [processState EXCEPT ![self] = "waiting"]
                            /\ runningProcess' = 0
                            /\ UNCHANGED <<readyProcesses, prioritiesQueue>>
                         \/ /\ CanRelease(self)
                            /\ processState' = [processState EXCEPT ![self] = "ready"]
                            /\ prioritiesQueue' = [prioritiesQueue EXCEPT ![ProcPriorities[self]] = Append(prioritiesQueue[ProcPriorities[self]], self)]
                            /\ readyProcesses' = readyProcesses + 1
                            /\ UNCHANGED runningProcess
                      /\ pc' = [pc EXCEPT ![self] = "advancedLoop"]

advancedBehaviour(self) == advancedLoop(self)

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == /\ \A self \in ProcSet: pc[self] = "Done"
               /\ UNCHANGED vars

Next == (\E self \in 1..(ProcessesBasic): baseBehaviour(self))
           \/ (\E self \in (ProcessesBasic + 1)..(ProcessesAdvanced + ProcessesBasic): advancedBehaviour(self))
           \/ Terminating

Spec == Init /\ [][Next]_vars

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION 
=============================================================================
